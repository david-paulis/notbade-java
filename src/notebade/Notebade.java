/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notebade;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author david
 */
public class Notebade extends Application {

    TextArea textArea;
   String selectedText="";
    @Override
    public void start(Stage primaryStage) {
        //navbar
        MenuBar bar = new MenuBar();
        Menu file = new Menu("File");
        MenuItem newItem = new MenuItem("New");
        newItem.setAccelerator(KeyCombination.keyCombination("Alt+n"));

        newItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                textArea.setText("");
            }
        });

        MenuItem openItem = new MenuItem("Open");
        openItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                FileInputStream fis = null;
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Open Resource File");
               File selectedFile= fileChooser.showOpenDialog(primaryStage);
               
            if (selectedFile != null) {
               String path= selectedFile.getAbsolutePath();
            try {
                    fis = new FileInputStream(path);
                    int size = fis.available();
                    byte[] b = new byte[size];
                    fis.read(b);
                    textArea.setText(new String(b));
                } catch (IOException ex) {
                    Logger.getLogger(Notebade.class.getName()).log(Level.SEVERE, null, ex);

                } finally {
                    try {
                        fis.close();
                    } catch (IOException ex) {
                        Logger.getLogger(Notebade.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
                ////////////
                
               

            }
        });
        MenuItem saveItem = new MenuItem("Save");
        saveItem.setAccelerator(KeyCombination.keyCombination("Alt+s"));
        saveItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                FileWriter fileWriter = null;
                PrintWriter printWriter = null;
                BufferedReader bufferedReader = null;
                 FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Open Resource File");
               File selectedFile= fileChooser.showOpenDialog(primaryStage);
                 if(selectedFile!=null){
               try {
                    fileWriter = new FileWriter("sample");
                    printWriter = new PrintWriter(fileWriter);
                    String data=textArea.getText();
                    printWriter.print(data);
                    
                } catch (IOException e) {
                    e.printStackTrace();
                } finally { //Closing the resources
                    try {
                        printWriter.close();
                        fileWriter.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }}
            }
        
    }
    );
        MenuItem exitItem = new MenuItem("Exit");
      exitItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Platform.exit();
            }
        });
    file.getItems ()
    .addAll(newItem,openItem,saveItem,exitItem);
        Menu edit = new Menu("Edit");
    MenuItem selectAllItem = new MenuItem("Select All");

    selectAllItem.setAccelerator (KeyCombination.keyCombination
    ("Alt+a"));
    selectAllItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                textArea.selectAll();
            }
        });
         MenuItem copyItem = new MenuItem("Copy");

    copyItem.setAccelerator (KeyCombination.keyCombination
    ("Alt+c"));
     copyItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                selectedText= textArea.getSelectedText();
               
            }
        });
   
         MenuItem cutItem = new MenuItem("Cut");

    cutItem.setAccelerator (KeyCombination.keyCombination
    ("Alt+x"));
     cutItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                selectedText= textArea.getSelectedText();
                textArea.deleteText(textArea.getSelection());
            }
        });
         MenuItem pastItem = new MenuItem("Past");

    pastItem.setAccelerator (KeyCombination.keyCombination
    ("Alt+v"));
    pastItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
//                selectedText= textArea.getSelectedText();
//                textArea.deleteText(textArea.getSelection());
                textArea.insertText(textArea.getAnchor(), selectedText);
            }
        });
         MenuItem undoItem = new MenuItem("Undo");
          
    undoItem.setAccelerator (KeyCombination.keyCombination
    ("Alt+z"));
    undoItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
//                selectedText= textArea.getSelectedText();
//                textArea.deleteText(textArea.getSelection());
                textArea.undo();
            }
        });
         MenuItem deleteItem = new MenuItem("Delete");
         deleteItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
//                selectedText= textArea.getSelectedText();
//                textArea.deleteText(textArea.getSelection());
               textArea.deleteText(textArea.getSelection());
            }
        });
    edit.getItems ()
    .addAll(selectAllItem,copyItem,cutItem,pastItem,undoItem,deleteItem);
         Menu help = new Menu("Help");
    MenuItem aboutItem = new MenuItem("about");
             aboutItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

               Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("About me");
                alert.setHeaderText(null);
                alert.setContentText("David Paulus , DevOps Engineer , My Skills: \n -Java \n -Ansible");
                alert.showAndWait();

            }
        });
    help.getItems ()

    .add(aboutItem);
    bar.getMenus ()
    .addAll(file,edit,help);
         BorderPane pane = new BorderPane();

    pane.setTop (bar);

    //textarea
    textArea  = new TextArea();

    pane.setCenter (textArea);
    Scene scene = new Scene(pane, 300, 300);

    primaryStage.setTitle (

    "Notebad");
    primaryStage.setScene (scene);

    primaryStage.show ();
}

/**
 * @param args the command line arguments
 */
public static void main(String[] args) {
        launch(args);
    }
    
}
